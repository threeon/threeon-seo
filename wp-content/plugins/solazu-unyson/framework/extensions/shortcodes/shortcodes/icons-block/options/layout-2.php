<?php

$style = array(
	esc_html__('Chennai', 'slz')    => 'st-chennai',
	esc_html__('Munbai', 'slz')     => 'st-mumbai',
	esc_html__('Pune', 'slz')       => 'st-pune',
	esc_html__('Jaipur', 'slz')     => 'st-jaipur',
	esc_html__('Noida', 'slz')      => 'st-noida',

);

$align = array(
	esc_html__('Left', 'slz')     => 'text-l',
	esc_html__('Right', 'slz')    => 'text-r',
	esc_html__('Center', 'slz')   => 'text-c',
);

$option_show = array(
	esc_html__('Normal', 'slz')      => 'normal',
	esc_html__('Option 1', 'slz')     => 'option-1',
	esc_html__('Option 2', 'slz')     => 'option-2',
	esc_html__('Option 3', 'slz')     => 'option-3',
	esc_html__('Option 4', 'slz')     => 'option-4',
);


$vc_options = array(
	array(
		'type'        => 'dropdown',
		'heading'     => esc_html__( 'Style', 'slz' ),
		'param_name'  => 'layout-2-style',
		'value'       => $style,
		'description' => esc_html__( 'Select style for blocks', 'slz' )
	),
	array(
		'type'        => 'dropdown',
		'heading'     => esc_html__( 'Block Align', 'slz' ),
		'param_name'  => 'layout-2-align',
		'value'       => $align,
		'description' => esc_html__( 'It is used for aligning the inner content of  blocks', 'slz' )
	),
	array(
		'type'        => 'colorpicker',
		'heading'     => esc_html__( 'Icon Background Color', 'slz' ),
		'param_name'  => 'icon_bg_cl_2',
		'value'       => '',
		'dependency'     => array(
			'element'  => 'layout-2-style',
			'value_not_equal_to'    => array('st-chennai')
		),
		'description' => esc_html__( 'Choose background color for icon of blocks.', 'slz' ),
		'group'       => esc_html__( 'Custom Color', 'slz' ),
	),
	array(
		'type'        => 'colorpicker',
		'heading'     => esc_html__( 'Icon Background Color (hover)', 'slz' ),
		'param_name'  => 'icon_bg_hv_cl_2',
		'value'       => '',
		'dependency'     => array(
			'element'  => 'layout-2-style',
			'value_not_equal_to'    => array('st-chennai')
		),
		'description' => esc_html__( 'Choose background color for icon when you mouse over it.', 'slz' ),
		'group'       => esc_html__( 'Custom Color', 'slz' ),
	),
	array(
		'type'        => 'colorpicker',
		'heading'     => esc_html__( 'Icon Border Color', 'slz' ),
		'param_name'  => 'icon_bd_cl_2',
		'value'       => '',
		'edit_field_class' => 'vc_col-sm-6 vc_column',
		'dependency'     => array(
			'element'  => 'layout-2-style',
			'value_not_equal_to'    => array('st-chennai')
		),
		'description' => esc_html__( 'Choose border color for icon of blocks.', 'slz' ),
		'group'       => esc_html__( 'Custom Color', 'slz' ),
	),
	array(
		'type'        => 'colorpicker',
		'heading'     => esc_html__( 'Icon Border Color (hover)', 'slz' ),
		'param_name'  => 'icon_bd_hv_cl_2',
		'value'       => '',
		'edit_field_class' => 'vc_col-sm-6 vc_column',
		'dependency'     => array(
			'element'  => 'layout-2-style',
			'value_not_equal_to'    => array('st-chennai')
		),
		'description' => esc_html__( 'Choose border color for icon when you mouse over it.', 'slz' ),
		'group'       => esc_html__( 'Custom Color', 'slz' ),
	),
);