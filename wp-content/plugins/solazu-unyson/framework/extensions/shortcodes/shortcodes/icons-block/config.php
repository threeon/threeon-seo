<?php

if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Forbidden' );
}

$cfg = array ();

$cfg ['page_builder'] = array (
		'title' => esc_html__( 'SLZ Icons Block', 'slz' ),
		'description' => esc_html__( 'Icons Block of info', 'slz' ),
		'tab' => slz()->theme->manifest->get('name'),
		'icon' => 'icon-slzcore-icons-block slz-vc-slzcore',
		'tag' => 'slz_icons_block' 
);

$cfg ['layouts'] = array (
	'layout-1' => esc_html__( 'United States', 'slz' ),
	'layout-2' => esc_html__( 'India', 'slz' ),
	'layout-3' => esc_html__( 'United Kingdom', 'slz' ),
	'layout-4' => esc_html__( 'Italy', 'slz' ),
);

$cfg ['layouts_map'] = array (
	'layout-1' => 'la-united-states',
	'layout-2' => 'la-india',
	'layout-3' => 'la-united-kingdom',
	'layout-4' => 'la-italy',
);

$icon_default = array();

for ($i = 1; $i <= 2; $i++) {

    $icon_default['icon_bg_cl_'.$i] = '';
    $icon_default['icon_bg_hv_cl_'.$i] = '';
    $icon_default['icon_bd_cl_'.$i] = '';
    $icon_default['icon_bd_hv_cl_'.$i] = '';
}


$cfg ['default_value'] =
	array_merge($icon_default, array (
	'layout'      	=> 'layout-1',
	'column'      	=> '',
	'layout-1-align'     => 'text-l',
	'layout-2-align'     => 'text-l',
	'layout-3-align'     => 'text-l',
	'option-show'   => '',
	'icon_size'   	=> '',
	'delay_animation'    => '0.5s',
	'item_animation'     => '',
	'icon_box'   	=>'',
	'button_link' 	=>'',
	'btn_text'    	=>'',
	'btn_cl'      	=>'',
	'btn_hv_cl'   	=>'',

	'layout-1-style' => 'st-florida',
	'layout-2-style' => 'st-chennai',
	'layout-3-style' => 'st-london',
	'layout-4-style' => 'st-milan',

    'icon_cl'     	=> '',
    'icon_hv_cl'  	=> '',
    'extra_class' 	=> '',
    
));

