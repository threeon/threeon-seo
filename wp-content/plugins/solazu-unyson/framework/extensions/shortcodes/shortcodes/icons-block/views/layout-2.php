<?php
$out = $icon_class = $under_line = $has_background = $custom_css = $css = '';
$i = 1;
$shortcode = slz_ext( 'shortcodes' )->get_shortcode('icons_block');

$param_default  = array(
	'block_bg_cl'            => '',
	'block_bg_hv_cl'         => '',
	'icon_type'                => '',
	'block_bg_img'             => '',
	'block_bg_hv_img'          => '',
	'img_up'                   => '',
	'title'                    => '',
	'des'                      => '',
);

switch ( $data['layout-2-style'] ) {

	case 'st-mumbai':
		$icon_class = 'icon-circle';
		break;
	case 'st-pune':
		$icon_class = 'icon-circle';
		$under_line = 'underline';
		break;
	case 'st-jaipur':
		$icon_class = 'icon-square';
		break;
	case 'st-noida':
		$icon_class = 'icon-square';
		$under_line = 'underline';
		break;
	default:
		break;
}

if ( !empty( $data['icon_box'] ) ) {
	$items = (array) vc_param_group_parse_atts( $data['icon_box'] );
	$out .= '<div class="slz-list-icon-block '. esc_attr( $data['column'] ) .' '.esc_attr($data['option-show']).' ">';
		foreach ( $items as $item ) {

			// background color
			if(!empty($item['block_bg_cl'])){
				$has_background = "has-background";
				$css = '
						.%1$s .slz-list-icon-block .icon-block-%2$s {
							background-color: %3$s;
						}
					';
				$custom_css .= sprintf( $css, esc_attr( $data['block_class'] ), esc_attr($i), esc_attr( $item['block_bg_cl']) );

			}
			// hover background color
			if(!empty($item['block_bg_hv_cl'])){
				$has_background = "has-background";
				$css = '
						.%1$s .slz-list-icon-block .icon-block-%2$s:hover {
							background-color: %3$s;
						}
					';
				$custom_css .= sprintf( $css, esc_attr( $data['block_class'] ), esc_attr($i), esc_attr( $item['block_bg_hv_cl']) );
			}
			$item = array_merge( $param_default, $item );
		
			$out .= '<div class="item">';
				$out .='<div class="slz-icon-block icon-block-'.esc_attr($i).' '.esc_attr($has_background).' '.esc_attr($data['layout-2-align']).'">';

					// -----------icon --------//
					$out .= '<div class="icon-cell '.esc_attr($icon_class).'">';
						if ( $item['icon_type'] == '02' ) {
							if ( !empty( $item['img_up'] ) && $img_url = wp_get_attachment_url( $item['img_up'] ) ) {
								$out .= '
									<div class="wrapper-icon-image">
										<img src="'.esc_url( $img_url ).'" alt="" class="slz-icon-img">
									</div>
								';
							}
						}else{
							$format = '<div class="wrapper-icon"><i class="slz-icon %1$s"></i></div>';
							$out .= $shortcode->get_icon_library_views( $item, $format );
						}
					$out .= '</div>';

					//-----------content--------//
					$out .= '<div class="content-cell">';
						$out .= '<div class="wrapper-info">';
							// title
							if( !empty($item['title']) ){
								$out .= '<div class="title '.esc_attr($under_line).'">'.esc_attr( $item['title'] ).'</div>';
							}
							// description
							if( !empty($item['des'])){
								$out .= '<div class="description">'.wp_kses_post( nl2br ($item['des'] ) ).'</div>';
							}
							
						$out .= '</div>';
					$out .= '</div>';
				$out .= '</div>';
				$i++;
			$out .= '</div>';

		}//end foreach

	$out .= '</div>';

	if ( !empty( $custom_css ) ) {
		do_action('slz_add_inline_style', $custom_css);
	}
}

echo $out;
?>
