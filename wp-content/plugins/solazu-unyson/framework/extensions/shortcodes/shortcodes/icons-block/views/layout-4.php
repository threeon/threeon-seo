<?php
$out = $icon_class = $under_line = '';
$shortcode = slz_ext( 'shortcodes' )->get_shortcode('icons_block');

$param_default  = array(
	'block_bg_cl_4'            => '',
	'block_bg_hv_cl_4'         => '',
	'icon_type'                => '',
	'block_bg_img'             => '',
	'block_bg_hv_img'          => '',
	'img_up'                   => '',
	'title'                    => '',
	'des'                      => '',
);


if ( !empty( $data['icon_box'] ) ) {
	$items = (array) vc_param_group_parse_atts( $data['icon_box'] );
	$out .= '<div class="slz-list-icon-block '. esc_attr( $data['column'] ) .'">';
		foreach ( $items as $item ) {

			$item = array_merge( $param_default, $item );
		
			$out .= '<div class="item">';
				$out .='<div class="slz-icon-block">';

					// -----------icon --------//
					$out .= '<div class="icon-cell '.esc_attr($icon_class).'">';
						if ( $item['icon_type'] == '02' ) {
							if ( !empty( $item['img_up'] ) && $img_url = wp_get_attachment_url( $item['img_up']) ) {
								$out .= '
									<div class="wrapper-icon-image">
										<img src="'.esc_url( $img_url ).'" alt="" class="slz-icon-img">
									</div>
								';
							}
						}else{
							$format = '<div class="wrapper-icon"><i class="slz-icon %1$s"></i></div>';
							$out .= $shortcode->get_icon_library_views( $item, $format );
						}
					$out .= '</div>';

					//-----------content--------//
					$out .= '<div class="content-cell">';
						$out .= '<div class="wrapper-info">';
							// title
							if( !empty($item['title']) ){
								$out .= '<div class="title '.esc_attr($under_line).'">'.esc_attr( $item['title'] ).'</div>';
							}
							// description
							if( !empty($item['des'])){
								$out .= '<div class="description">'.wp_kses_post( nl2br ($item['des'] ) ).'</div>';
							}
							
						$out .= '</div>';
					$out .= '</div>';
				$out .= '</div>';
			$out .= '</div>';

		}//end foreach

	$out .= '</div>';
}

echo $out;
?>
