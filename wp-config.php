<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'threeon_seo');

/** MySQL database username */
define('DB_USER', 'threeon');

/** MySQL database password */
define('DB_PASSWORD', '30nPass!');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^aBAaD,/C+WBe#az.99#f-O6Nf66blDr-86A^BcSQuny(HS]yLqCCi:v:Y<AtJ}=');
define('SECURE_AUTH_KEY',  'Zp.6>O`fR<(d6(17GB3DVMb(R Ao9VhJD-i3^b!(6BGnn KQz_1J.VQ{)O@:+9= ');
define('LOGGED_IN_KEY',    'U>$l?`*90E*,*Q&|PIeqq2o&]{M+SEor-7KnUgDy<$$KR`LDt9aY~undi$,)E-W%');
define('NONCE_KEY',        '@=`YY U=vO8xc>wgit,!?jw.7i_{OQrA,|Ul=PECTC..)kC_vAB.Vpg{So_*PtS}');
define('AUTH_SALT',        'D)g_Eq/-kmY]49t$iI_ttr(u;M_K4SeWYlUDxbI=n[Hb` [u1e>-V}ma#&wWUD^A');
define('SECURE_AUTH_SALT', 'Q++M]~os(>pAOd@@jbm2d#:67K=/|A|%SLCb`9P&*ZNZW~)_bm !)vtuPt4U9%0:');
define('LOGGED_IN_SALT',   'S k#ooet1Xk],2_VeuC/V&f3;u__vXxG.eM)M^e%v<gkDto9 pXXBoj+Ic*}Py=Y');
define('NONCE_SALT',       '1.c7168Z5?_ru_S8,KH@iX>WNzlk~!*[j@.#^+w;n)C)3Z.j#0CieoaK3>(gkWy.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
